#! /bin/bash
#
# Escola del Treball de Barcelona
# Administració de Sistemes Informàtics
# Curs 2021/2022
#
# Autor: Isaac Gordillo
# Data: 09/02/2022
#
# Descripció:
#
# Validar si l'argument introduït és un regular file, un directori
# o un link. Si és cap d'aquestes opcions, es tanca el programa.
# Si ho és, es llista.
#

# 1.- Se validan los argumentos.

# Si la cantidad de argumentos introducidos es igual a 0,

ERR_ARGS=1
ERR_NODIR=2

if [ $# -eq 0 ]
then
	
  # se muestran por pantalla textos en caso de error
  
  echo "ERROR: Nombre d'arguments incorrecte."
  echo "Usage: $0 directori"

  # y se cierra el programa.

  exit $ERR_NARGS
fi

# 2.- Se valida que el argumento cumpla condiciones.

# Se introduce el argumento.

fit=$1

# Si el argumento no es un regular file, un directorio o un link,

if ! [ -f $fit -o -L $fit -o -d $fit ]
then

  # se muestra un mensaje de error.

  echo "ERROR: L'argument introduït ha de ser un regular file, un directori o un link."
  echo "Usage: $0 directori"
  exit $ERR_NODIR
fi

# 3.- El programa en sí.

# Si la nota introducida es mayor o igual a 5 se aprueba.
  # Si la nota es un 6, es un bien.
  # Si la nota es 7 u 8, es un notable.
  # Si es un 9 o un 10, es un excelente.
# Si no, se suspende.

if [ -f $fit ]
then
  echo "L'argument introduït ($fit) és un regular file."
  ls $1

elif [ -L $fit ]
then
  echo "L'argument introduït ($fit) és un link."
  ls $1

else
  echo "L'argument introduït ($fit) és un directori."
  ls $1
fi
exit 0
