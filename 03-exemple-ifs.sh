#! /bin/bash
#
# Escola del Treball de Barcelona
# Administració de Sistemes Informàtics
# Curs 2021/2022
#
# Autor: Isaac Gordillo
# Data: 07/02/2022
#
# Descripció:
#
# Ifs.
#

# 1.- Se validan los argumentos.

# Si la cantidad de argumentos introducidos es diferente de 1,

if [ $# -ne 1 ]
then
	
  # se muestran por pantalla varios textos.
  
  echo "Error: nombre d'arguments incorrecte."
  echo "Usage: $0 edat"
  exit 1
fi

# 2.- 

edat=$1

if [ $edat -ge 18 ]
then
  echo "L'edat introduïda ($edat) és major d'edat."
else
  echo "L'edat introduïda ($edat) és menor d'edat."
fi
exit 0
