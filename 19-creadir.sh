#! /bin/bash
#
# Escola del Treball de Barcelona
# Administració de Sistemes Informàtics
# Curs 2021/2022
#
# Autor: Isaac Gordillo
# Data: 23/02/2022
#
# Descripció:
#
# Crear n directoris amb almenys un argument y sense generar cap sortida.
# Si s'han pogut crear tots el directoris, el programa retorna un 0;
# si hi ha error en el nombre d'arguments, el programa retorna un 1 i,
# si algun dir no s'ha pogut crear, el programa retorna un 2.
#

# 1.- Se validan los argumentos.

# Si la cantidad de argumentos introducidos es igual a 0,

status=0
ERR_ARGS=1
ERR_MKDIR=2

if [ $# -lt 1 ]
then
	
  # se muestran por pantalla textos en caso de error
  
  echo "ERROR: Nombre d'arguments incorrecte."
  echo "Usage: $0 nomdir[...]."

  # y se cierra el programa.

  exit $status
fi

dir=$1

for dir in $*
do
  mkdir $dir &> /dev/null
  if [ $? -ne 0 ]
  then
    status=$ERR_MKDIR
    echo "ERROR: No s'ha creat $dir." >&2
  fi
done
exit 0
