#! /bin/bash
#
# Escola del Treball de Barcelona
# Administració de Sistemes Informàtics
# Curs 2021/2022
#
# Autor: Isaac Gordillo
# Data: 07/02/2022
#
# Descripció:
#
# Suspès, aprovat, notable o excel·lent. S'ha de validar l'argument i el seu rang [0, 10].
#

# 1.- Se validan los argumentos.

# Si la cantidad de argumentos introducidos es diferente de 1,

ERR_ARGS=1
ERR_NOTA=2

if [ $# -ne 1 ]; then
	
  # se muestran por pantalla textos en caso de error
  
  echo "ERROR: Nombre d'arguments incorrecte."
  echo "Usage: $0 nota"

  # y se cierra el programa.

  exit $ERR_ARGS
fi

# 2.- Se valida el rango del argumento.

# Se introduce el argumento.

nota=$1

# Si la nota no pertenece al intervalo [0, 10],

if [ $nota -lt 0 -o $nota -gt 10 ]; then

  # Se muestra un mensaje de error.

  echo "ERROR: La nota introduïda ($nota) ha de ser un nombre entre 0 i 10."
  echo "Usage: $0 nota"
  exit $ERR_NOTA
fi

# 3.- El programa en sí.

# Si la nota introducida es mayor o igual a 5 se aprueba.
  # Si la nota es un 6, es un bien.
  # Si la nota es 7 u 8, es un notable.
  # Si es un 9 o un 10, es un excelente.
# Si no, se suspende.

if [ $nota -lt 5 ]; then
  echo "La nota introduïda ($nota) és un suspès."
elif [ $nota -eq 5 ]; then
  echo "La nota introduïda ($nota) és un aprovat."
elif [ $nota -eq 6 ]; then
  echo "La nota introduïda ($nota) és un bé."
elif [ $nota -eq 7 -o $nota -eq 8 ]; then
  echo "La nota introduïda ($nota) és un notable."
else
  echo "La nota introduïda ($nota) és un excel·lent."
fi
exit 0
