#! /bin/bash
#
# Escola del Treball de Barcelona
# Administració de Sistemes Informàtics
# Curs 2021/2022
#
# Autor: Isaac Gordillo
# Data: 14/02/2022
#
# Descripció:
#
# Exemples bucle for.
#

# Listar, numerados, los logins.

lista_nombres=$(cut -d: -f1 /etc/passwd | sort)
num=1

for elem in $lista_nombres
do
  echo "$num: $elem"
  ((num++))
done
exit 0

# Listar, numerados, los ficheros del directorio activo.

lista_nombres=$(ls)
num=1

for elem in $lista_nombres
do
  echo "$num: $elem"
  ((num++))
done
exit 0

# Iterar por la lista de argumentos y numerarlos.

num=0

for arg in $*
do
  num=$(($num+1))
  echo "$num: $arg"
done
exit 0

# Iterar por la lista de argumentos.

for arg in "$@"
do
  echo $arg
done
exit 0


# Iterar por la lista de argumentos.

for arg in "$*"
do
  echo $arg
done
exit 0

# Iterar por la lista de argumentos.

for arg in $*
do
  echo $arg
done
exit 0

# Iterar para el valor de una variable.

lista=$(ls)
for nom in $lista
do
  echo $nom
done
exit 0

# Iterar para un solo elemento.

ERR_NARGS=1
ERR_MES=2
mes=$1

for nom in "pere pau marta anna"
do
  echo "$nom"
done
exit 0

# Iterar para un conjunto de elementos.

ERR_NARGS=1
ERR_MES=2
mes=$1

for nom in "pere" "pau" "marta" "anna"
do
  echo "$nom"
done
exit 0
