#! /bin/bash
#
# Escola del Treball de Barcelona
# Administració de Sistemes Informàtics
# Curs 2021/2022
#
# Autor: Isaac Gordillo
# Data: 14/02/2022
#
# Descripció:
#
# Es rep una nota o més. Per cada nota dir
# si és suspès. aprovat, notable, excel·lent.
#

# 1.- Se validan los argumentos.

# Si la cantidad de argumentos introducidos es menor a 1,

ERR_ARGS=1
ERR_NOTA=2

if [ $# -lt 1 ]
then
	
  # se muestran por pantalla textos en caso de error
  
  echo "ERROR: Nombre d'arguments incorrecte."
  echo "Usage: $0 nota"

  # y se cierra el programa.

  exit $ERR_ARGS
fi

# 2.- El programa en sí.

# Si la nota introducida es mayor o igual a 5 se aprueba.
# Si no, se suspende.

for nota in $*
do

   # Se valida el rango del argumento.
   # Si la nota no pertenece al intervalo [0, 10],

  if [ $nota -lt 0 -o $nota -gt 10 ]
  then

    # se muestra un mensaje de error.

    echo "ERROR: la nota introduïda ($nota) ha de ser un nombre entre 0 i 10." >> /dev/stderr

  elif [ $nota -lt 5 ]
  then
    echo "La nota introduïda ($nota) és un suspès."
  elif [ $nota -eq 5 ]
  then
    echo "La nota introduïda ($nota) és un aprovat."
  elif [ $nota -eq 6 ]
  then
    echo "La nota introduïda ($nota) és un bé."
  elif [ $nota -eq 7 -o $nota -eq 8 ]
  then
    echo "La nota introduïda ($nota) és un notable."
  else
    echo "La nota introduïda ($nota) és un excel·lent."
  fi
done
exit 0
