#! /bin/bash
#
# Escola del Treball de Barcelona
# Administració de Sistemes Informàtics
# Curs 2021/2022
#
# Autor: Isaac Gordillo
# Data: 09/02/2022
#
# Descripció:
#
# Validar si l'argument introduït és un directori o no.
# Si no ho és, es tanca el programa. Si ho és, es llista.
#

# 1.- Se validan los argumentos.

# Si la cantidad de argumentos introducidos es igual a 0,

ERR_ARGS=1
ERR_NODIR=2

if [ $# -eq 0 ]
then
	
  # se muestran por pantalla textos en caso de error
  
  echo "ERROR: Nombre d'arguments incorrecte."
  echo "Usage: $0 directori"

  # y se cierra el programa.

  exit $ERR_NARGS
fi

# 2.- Se valida que el argumento cumpla condiciones.

# Se introduce el argumento.

dir=$1

# Si la nota no pertenece al intervalo [0, 10],

if ! [ -d $dir ]
then

  # Se muestra un mensaje de error.

  echo "ERROR: L'argument introduït ha de ser un directori."
  echo "Usage: $0 directori"
  exit $ERR_NODIR
fi

# 2.- Si se pide help, se muestra y luego se cierra el programa.

if [ "$1" = "-h" -o "$1" = "--help" ]; then
   

# 3.- El programa en sí.

# Si la nota introducida es mayor o igual a 5 se aprueba.
  # Si la nota es un 6, es un bien.
  # Si la nota es 7 u 8, es un notable.
  # Si es un 9 o un 10, es un excelente.
# Si no, se suspende.

else
  ls $dir
exit 0
