#! /bin/bash
#
# Escola del Treball de Barcelona
# Administració de Sistemes Informàtics
# Curs 2021/2022
#
# Autor: Isaac Gordillo
# Data: 21/02/2022
#
# Descripció:
#
# Validar si existeixen dos o més arguments, dir si l'últim és un directori
# i, després de dir si els n primers arguments són files, copiar els que ho
# són al directori.
#

# 1.- Se validan los argumentos.

# Si la cantidad de argumentos introducidos es menor que 2,

ERR_ARGS=1
ERR_NODIR=2

if [ $# -lt 2 ]
then
	
  # se muestran por pantalla textos en caso de error
  
  echo "ERROR: Nombre d'arguments incorrecte."
  echo "Usage: $0 file [...] dir-destí"

  # y se cierra el programa.

  exit $ERR_NARGS
fi

# 2.- Se valida que el primer argumento sea un file o no y
#     se muestra por pantalla el resultado

# Se introduce el argumento.

desti=$(echo $* | sed 's/^.* //') # o bien desti=$(echo $* | cut -d' ' -f$#)
llista_file=$(echo $* | sed 's/ [^ ]*$//')

# 3.- Decir si el dir existe o no.

if ! [ -d $desti ]
then
  echo "ERROR: $desti no és un directori."
  echo "Usage $0 file dir-destí"
  exit $ERR_NODIR
else
  echo "$desti és un file."
fi

# 3.- Se valida que el segundo argumento sea un dir existente.
# Se introduce el argumento.

arg2=$2

if ! [ -d $arg2 ]
then
  echo "$arg2 no és un directori existent."
else
  echo "$arg2 és un directori existent."
fi
cp $arg1 $arg2
exit 0
