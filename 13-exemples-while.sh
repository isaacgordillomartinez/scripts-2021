#! /bin/bash
#
# Escola del Treball de Barcelona
# Administració de Sistemes Informàtics
# Curs 2021/2022
#
# Autor: Isaac Gordillo
# Data: 14/02/2022
#
# Descripció:
#
# Exemples bucle while.
#

# Numerar l'entrada estàndard línia a línia i passar a majúscules.

num=1

while read -r line
do
  echo "$num: $line " | tr 'a-z' 'A-Z'
  ((num++))
done
exit 0

# Processar l'entrada estàndard fins el token "FI".

read -r line
while [ "$line" != "FI" ] 
do
  echo "$line"
  read -r line
done
exit 0


# Numerar stdin línia a línia.

num=1

while read -r line
do
  echo $"$num: line "
  ((num++))
done
exit 0

# Processar l'entrada estàndard (<) línia a línia.

while read -r line
do
  echo $line
done
exit 0

# Iterar arguments amb shift.

while [ -n "$1" ]
do
  echo "$1, $#, $*"
  shift
done
exit 0

# Mostrar un comptador decreixent de l'argument [n, 0].

MIN=0
num=$1

while [ $num -ge $MIN ]
do
  echo $num
  ((num--))
done
exit 0

# Mostrar un comptador de l'1 a MAX.

MAX=10
num=1

while [ $num -le $MAX ]
do
  echo $num
  ((num++))
done

# Sinaxis

# while condición
# do
#   acciones
# done
