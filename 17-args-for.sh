#! /bin/bash
#
# Escola del Treball de Barcelona
# Administració de Sistemes Informàtics
# Curs 2021/2022
#
# Autor: Isaac Gordillo
# Data: 21/02/2022
#
# Descripció:
#
# Validar quins arguments i quines opcions té una ordre.
#

# 1.- Se validan los argumentos.

# Si la cantidad de argumentos introducidos es igual a 0,

ERR_ARGS=1
ERR_NODIR=2

if [ $# -eq 0 ]
then
	
  # se muestran por pantalla textos en caso de error
  
  echo "ERROR: Nombre d'arguments incorrecte."
  echo "Usage: $0 [-a -b -c -d -e -f] arg[...]"

  # y se cierra el programa.

  exit $ERR_NARGS
fi

# 2.- Se valida que el primer argumento sea un file o no y

opcions=""
arguments=""

for arg in $*
do
  case $opcions in

  "-a"|"-b"|"-s"|"-r"|"-i")
    opcions="$opcions $arg"
    ;;
  *)
    echo "Opcions no vàlides."
    arguments="$arguments $arg"
  esac
done
exit 0
