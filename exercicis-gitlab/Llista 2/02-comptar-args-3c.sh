#! /bin/bash
#
# Escola del Treball de Barcelona
# Administració de Sistemes Informàtics
# Curs 2021/2022
#
# Autor: Isaac Gordillo
# Data: 03/03/2022
#
# Descripció:
#
# Processar els arguments i comptar quants n'hi de 3 o més caràcters.
#

ERR_ARGS=1

if [ $# -lt 1 ]
then
  echo "ERROR: Nombre d'arguments incorrecte."
  echo "Usage: $0 argument."
  exit $ERR_ARGS
fi

comptador=0

for arg in $*
do
  caracters=$(echo -n $arg | wc -m)
  if [ $caracters -ge 3 ]; then
    ((comptador++))
  fi
done
echo "Hi ha $comptador arguments amb 3 o més caràcters."
exit 0
