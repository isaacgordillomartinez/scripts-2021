#! /bin/bash
#
# Escola del Treball de Barcelona
# Administració de Sistemes Informàtics
# Curs 2021/2022
#
# Autor: Isaac Gordillo
# Data: 17/02/2022
#
# Descripció:
#
# Mostrar stdin numerant línia a línia.
#

num=1
while read -r line
do
  echo $"$num: $line "
  ((num++))
done
exit 0
