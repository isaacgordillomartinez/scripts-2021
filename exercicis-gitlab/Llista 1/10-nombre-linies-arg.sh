#! /bin/bash
#
# Escola del Treball de Barcelona
# Administració de Sistemes Informàtics
# Curs 2021/2022
#
# Autor: Isaac Gordillo
# Data: 02/03/2022
#
# Descripció:
#
# Rebre com argument un nombre, processar stdin línia a
# línia i mostrar numerades el nombre donat de línies.
#

ERR_ARGS=1
ERR_MAX=2

if [ $# -ne 1 ]
then
  echo "ERROR: Nombre d'arguments incorrecte."
  echo "Usage: $0 arg."
  exit $ERR_ARGS
fi

MAX=$1
comptador=0

if [ $MAX -lt 1 ]
then
  echo "ERROR: L'argument ha de ser un nombre major que 1."
  echo "Usage: $0 max."
  exit $ERR_MAX
fi

while read -r line
do
  ((comptador++))
  echo "$comptador: $line"
  if [ $comptador -eq $MAX ]
  then
    exit 0
  fi
done
