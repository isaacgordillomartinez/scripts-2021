#! /bin/bash
#
# Escola del Treball de Barcelona
# Administració de Sistemes Informàtics
# Curs 2021/2022
#
# Autor: Isaac Gordillo
# Data: 24/02/2022
#
# Descripció:
#
# Rebre per com arguments usernames. Si no existeixen (no es troben
# a /etc/passwd), es mostren per stderr. Si no, per stdout.
#

for arg in $*
do
  grep "^$arg:" /etc/passwd
  if [ $? -eq 0 ]
  then
    echo $arg
  else
    echo "ERROR: l'usuari $arg no existeix." >> /dev/stderr
  fi
done
exit 0
