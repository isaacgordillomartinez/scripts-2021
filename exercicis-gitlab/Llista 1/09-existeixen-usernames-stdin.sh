#! /bin/bash
#
# Escola del Treball de Barcelona
# Administració de Sistemes Informàtics
# Curs 2021/2022
#
# Autor: Isaac Gordillo
# Data: 24/02/2022
#
# Descripció:
#
# Rebre per stdin usernames. Si no existeixen (no es troben
# a /etc/passwd), es mostren per stderr. Si no, per stdout.
#

while read -r line
do
  grep "^$line:" /etc/passwd
  if [ $? -eq 0 ]
  then
    echo $line
  else
    echo "ERROR: l'usuari $line no existeix." >> /dev/stderr
  fi
done
exit 0
