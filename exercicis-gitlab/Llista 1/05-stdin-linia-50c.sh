#! /bin/bash
#
# Escola del Treball de Barcelona
# Administració de Sistemes Informàtics
# Curs 2021/2022
#
# Autor: Isaac Gordillo
# Data: 24/02/2022
#
# Descripció:
#
# Mostrar línia a línia l'entrada estàndard, retallant només
# els 50 primers caràcters.
#

while read -r line
do
  echo $line | cut -c50-
done
exit 0
