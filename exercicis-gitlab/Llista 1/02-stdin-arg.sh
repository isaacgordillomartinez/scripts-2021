#! /bin/bash
#
# Escola del Treball de Barcelona
# Administració de Sistemes Informàtics
# Curs 2021/2022
#
# Autor: Isaac Gordillo
# Data: 17/02/2022
#
# Descripció:
#
# Mostrar els arguments rebuts línia a línia, tot numerant-los.
#

# 1.- Se validan los argumentos.

# Si la cantidad de argumentos introducidos es menor que 1,

ERR_ARGS=1

if [ $# -lt 1 ]
then
	
  # se muestran por pantalla textos de error
  
  echo "ERROR: Nombre d'arguments incorrecte."
  echo "Usage: $0 argument."

  # y se cierra el programa.

  exit $ERR_ARGS
fi

# 2.- Xixa.

num=0

for arg in $*
do
  num=$(($num+1))
  echo "$num: $arg"
done
exit 0
