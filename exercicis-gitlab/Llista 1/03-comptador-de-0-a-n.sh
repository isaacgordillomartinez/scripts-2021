#! /bin/bash
#
# Escola del Treball de Barcelona
# Administració de Sistemes Informàtics
# Curs 2021/2022
#
# Autor: Isaac Gordillo
# Data: 17/02/2022
#
# Descripció:
#
# Fer un comptador des de zero fins el valor indicat per
# l'argument rebut.
#

# 1.- Se validan los argumentos.

# Si la cantidad de argumentos introducidos es diferente de 1,

ERR_ARGS=1

if [ $# -ne 1 ]
then
	
  # se muestran por pantalla textos de error
  
  echo "ERROR: Nombre d'arguments incorrecte."
  echo "Usage: $0 nombre."

  # y se cierra el programa.

  exit $ERR_ARGS
fi

# 2.- Xixa.

MAX=$1
num=0

if [ "$MAX" -lt 0 ]
then
  while [ $num -ge $MAX ]
  do
    echo $num
    ((num--))
  done
exit 0

else # Esto ya no hace falta, en teoría.

  while [ $num -le $MAX ]
  do
    echo $num
    ((num++))
  done
fi
exit 0
