#! /bin/bash
#
# Escola del Treball de Barcelona
# Administració de Sistemes Informàtics
# Curs 2021/2022
#
# Autor: Isaac Gordillo
# Data: 17/02/2022
#
# Descripció:
#
# Es reben com a arguments un o més nombres de mes i s'indica, per
# a cada mes, quants dies té.
#

# 1.- Se valida el número de argumentos.

ERR_NARGS=1

if [ $# -lt 1 ]
then

  # se muestran por pantalla textos de error
  
  echo "ERROR: nombre d'arguments incorrecte."
  echo "Usage: $0 mes"

  # y se cierra el programa.

  exit $ERR_NARGS
fi

# 2.- Determinar la cantidad de días que tiene un mes.

for mes in $*
do
  if [ $mes -lt 1 -o $mes -gt 12 ]
  then

    # se muestra un mensaje de error.

    echo "ERROR: El mes $mes ha de ser un nombre entre 1 i 12." >> /dev/stderr
  else
    case $mes in

      "1"|"3"|"5"|"7"|"8"|"10"|"12")
        echo "El mes $mes té 31 dies."
        ;;
      "2")
        echo "El mes $mes té 28 dies."
        ;;
      *)
        echo "El mes $mes té 30 dies."
    esac
  fi
done
exit 0
