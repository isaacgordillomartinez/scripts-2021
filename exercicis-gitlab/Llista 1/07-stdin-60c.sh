#! /bin/bash
#
# Escola del Treball de Barcelona
# Administració de Sistemes Informàtics
# Curs 2021/2022
#
# Autor: Isaac Gordillo
# Data: 24/02/2022
#
# Descripció:
#
# Processar línia a línia l'entrada estàndard. Si la línia
# té més de 60 caràcters, es mostra, si no, no.
#
while read -r line
do
  if [ $(echo $line | wc -m) -gt 60 ]
  then
    echo $line
  fi
done
exit 0
