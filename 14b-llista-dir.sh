#! /bin/bash
#
# Escola del Treball de Barcelona
# Administració de Sistemes Informàtics
# Curs 2021/2022
#
# Autor: Isaac Gordillo
# Data: 16/02/2022
#
# Descripció:
#
# Validar si existeix un argument, dir si aquest és un directori
# i fer un ls del directori.
#

# 1.- Se validan los argumentos.

# Si la cantidad de argumentos introducidos es igual a 0,

ERR_ARGS=1
ERR_NODIR=2

if [ $# -ne 1 ]
then
	
  # se muestran por pantalla textos en caso de error
  
  echo "ERROR: Nombre d'arguments incorrecte."
  echo "Usage: $0 directori"

  # y se cierra el programa.

  exit $ERR_NARGS
fi

# 2.- Se valida que el argumento cumpla condiciones.

# Se introduce el argumento.

dir=$1

# Si el argumento introducido no es un directorio,

if ! [ -d $dir ]
then

  # se muestra un mensaje de error.

  echo "ERROR: L'argument introduït ha de ser un directori."
  echo "Usage: $0 directori"
  exit $ERR_NODIR
fi

# 3.- Hacer un ls del dir.

llista=$(ls $dir)
posicio=1

for element in $llista
do
  echo "$posicio: $element"
  ((posicio++))
done
exit 0
