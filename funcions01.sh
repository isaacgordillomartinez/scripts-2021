#! /bin/bash
#
# Escola del Treball de Barcelona
# Administració de Sistemes Informàtics
# Curs 2021/2022
#
# Autor: Isaac Gordillo
# Data: 25/04/2022
#
# Descripció:
#
# Primera función.
#

function hola(){
  echo "hola"
  return 0
}

function dia(){
  date
  return 0
}

function suma(){
  echo $(($1+$2))
  return 0
}

function all(){
  hola
  echo "Avui és: $(dia)."
  suma 6 9
}


### 1.- fsize

# Dado un login, calcular con la orden du lo que ocupa
# el home del usuario. Hace falta obtener el home del /etc/passwd.

function fsize(){

  # 1. Validar que existe el login.

  login=$1
  linia=$(grep "^$login:" /etc/passwd)
  if [ -z "$linia" ]; then
    echo "Error: L'usuari $login no existeix."
    return 1
  fi

  # Probar que esto funciona.

  echo $linia

  # 2. Obtener su homedir.

  dirHome=$(echo $linia | cut -d: -f6)
  du -sh $dirHome

  # 3. Calcular su du -sh.
  du -sh $dirHome
}

### 2.- loginargs login

# Recibir logins y, para cada login se muestra lo que ocupa
# el home del usuario usando fsize.

function loginargs(){
  # 1. Validar que al menos se recibe un 1 login.
  if [ $# -eq 0 ]; then
    echo "Error"
    return 1
  fi
  # 2. Iterar por cada login.
  for login in $*
  do
    fsize $login
  done
}

### 3.- loginfile file

# Recibir como argumento un nombre de fichero que contiene
# un login por línea. Muestra la ocupación de disco de cada
# usuario usando fsize. Verifica que se recibe un argumento
# y que este es un regular file.

function loginfile(){
  # Validar que se recibe un argumento.
  if [ ! -f "$1" ]; then
    echo "No existeix el fitxer $1."
    return 1
  fi
  fileIn=$1
  while read -r login
  do
    fsize $login
  done < $fileIn
}

# Maneras de procesar la entrada estándar:

# programa (signo de botón de ENTER)
# programa < file
# cat file | programa
# while read -r line
# do
#   cosas
# done

# OTRA FORMA:

# while read -r line
# do
#   cosas
# done < $1  # $1 ha de ser el nombre de un fichero.

### 4.- loginboth file

# Procesa un file si se le pasa uno o, si no se hace stdin.
# Luego muestra fsize de los usuarios.

function loginboth(){
  fileIn=/dev/stdin
  if [ $# -eq 1 ]; then
    fileIn=$1
  fi
  # Procesar la entrada estándar.
  while read -r file
  do
    fsize $login
  done < $fileIn
}

### 5.- grepgid gid

# Recibir un GID. Se valida que recibe un argumento y que este
# es un GID válido. Devuelve la lista de logins que tienen este
# grupo como grupo principal.

function getgid(){
  # Validar que se recibe un argumento.
    if [ $# -ne 1 ]; then
      echo "ERROR_ARGS: No hi ha arguments."
      return 1
    fi
    gid=$1
    grep -q "^[^:]*:[^:]*:$gid:" /etc/group
    if [ $? -ne 0 ]; then
      echo "ERROR_GID: GID $gid inexistent."
      return 2
    fi
    cut -d: -f1,4 /etc/passwd | grep ":$gid$" | cut -d: -f1
}

### 6.- gidsize gid

# Dado un GID como argumento, mostrar para cada usuario que pertenece
# a este grupo la ocupación de disco de su home.

function gidsize(){
  gid=$1
  llista_logins=$(getgid $gid)
  for login in $llista_login
  do
    fsize $login
  done
}

### 7.- allgidsize

# Informe de todos los gids listando la ocupación de sus usuarios.

function allgidsize(){
  llista_gida=$(cut -d: -f4 /etc/passwd | sort -gu)
  for gid in $llista_gids
  do
    echo "GID: $gid ----------"
  gidsize $gid
  done
}

### 8.- filtergroup

# Listar las líneas del fichero /etc/passwd de los usuarios que
# pertenecen a los grupos del 0 al 100 ambos inclusive.

function filtergroup(){
  file=passwd.txt
  while read -r line /etc/passwd
  do
    gid=$
  done
  if [  ]; then
  fi
}

### 9.- fstype

# Dado un tipo de filesystem como argumento, lista el device y el
# mount point (por orden de device) de las entradas del fstab de
# dicho fstype.

function fstype(){

}

### 10.- allfstype

# Listar para cada fstype que existe en el fstab las entradas del
# fstype dado por argumento (según entiendo) en orden lexicográfico
# Listar también, de forma tabulada, el device y el mountpoint.

function allfstype(){

}

### 11.- allfstypeif

# Lo mismo de antes pero recibiendo un valor numérico como argumento
# que indica el número mínimo de entradas de este fstype que tiene
# que haber para salir en el listado.

function allfstypeif(){

}

# git add.; git commit -m "scripts" ; git push
