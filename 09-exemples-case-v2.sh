#! /bin/bash
#
# Escola del Treball de Barcelona
# Administració de Sistemes Informàtics
# Curs 2021/2022
#
# Autor: Isaac Gordillo
# Data: 09/02/2022
#
# Descripció:
#
# Validar si un dia és laborable o no.
#

case $1 in
  "dl"|"dt"|"dc"|"dj"|"dv")
    echo "És laborable."
    ;;
  "ds"|"dm")
    echo "És festiu."
    ;;
  *)
    echo "És indefinit."
esac
