#! /bin/bash
#
# Escola del Treball de Barcelona
# Administració de Sistemes Informàtics
# Curs 2021/2022
#
# Autor: Isaac Gordillo
# Data: 21/02/2022
#
# Descripció:
#
# Validar quins arguments i quines opcions té una ordre.
#

# 1.- Se validan los argumentos.

# Si la cantidad de argumentos introducidos es igual a 0,

ERR_ARGS=1
ERR_NODIR=2

if [ $# -eq 0 ]
then
	
  # se muestran por pantalla textos en caso de error
  
  echo "ERROR: Nombre d'arguments incorrecte."
  echo "Usage: $0 [-a -b -c -d -e -f] arg[...]"

  # y se cierra el programa.

  exit $ERR_NARGS
fi

# 2.- Se valida que el primer argumento sea un file o no y

opcions=""
arguments=""
fitxer=""
num=""

while [ "$1" ]
do
  case $1 in

  "-b"|"-c"|"-e")
    opcions="$opcions $1"
    ;;
  "-a")
    opcions="$opcions $1"
    fitxer=$2
    shift
    ;;  
  "-d")
    opcions="$opcions $1"
    num=$2
    shift
    ;;
  *)
    echo "ERROR: opcions $opcions no vàlides."
    arguments="$arguments $1"
  esac
  shift
done
echo "Opcions: $opcions"
echo "Arguments: $arguments"
exit 0
