#! /bin/bash
#
# Escola del Treball de Barcelona
# Administració de Sistemes Informàtics
# Curs 2021/2022
#
# Autor: Isaac Gordillo
# Data: 21/02/2022
#
# Descripció:
#
# Validar si existeixen dos arguments, dir si el primer és un file
# i si el segon és un directori existent.
#

# 1.- Se validan los argumentos.

# Si la cantidad de argumentos introducidos es igual a 2,

ERR_ARGS=1
ERR_NODIR=2

if [ $# -ne 2 ]
then
	
  # se muestran por pantalla textos en caso de error
  
  echo "ERROR: Nombre d'arguments incorrecte."
  echo "Usage: $0 directori"

  # y se cierra el programa.

  exit $ERR_NARGS
fi

# 2.- Se valida que el primer argumento sea un file o no y
#     se muestra por pantalla el resultado

# Se introduce el argumento.

arg1=$1

if ! [ -f $arg1 ]
then
  echo "$arg1 no és un file."
else
  echo "$arg1 és un file."
fi

# 3.- Se valida que el segundo argumento sea un dir existente.
# Se introduce el argumento.

arg2=$2

if ! [ -d $arg2 ]
then
  echo "$arg2 no és un directori existent."
else
  echo "$arg2 és un directori existent."
fi
cp $arg1 $arg2
exit 0
