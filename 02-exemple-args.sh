#! /bin/bash
#
# Isaac Gordillo
# Febrer 2022
#
#
# Exemple de primer programa
# Normes:
#	shebang
#	capçalera: descripció, data i autor
#
#----------------------------------------------------
#

echo '$*: ' $*
echo '$@: ' $@
echo '$#: ' $#
echo '$1: ' $1
echo '$2: ' $2
echo '$9: ' $9
echo '$10: ' ${10}
echo '$11: ' ${11}
echo '$$: ' $$
nom='puig'
# echo "$nomdeworld" != echo "${nom}deworld"
echo "${nom}deworld"
exit 0
