#! /bin/bash
#
# Escola del Treball de Barcelona
# Administració de Sistemes Informàtics
# Curs 2021/2022
#
# Autor: Isaac Gordillo
# Data: 07/02/2022
#
# Descripció:
#
# Suspès o aprovat. S'ha de validar l'argument i el seu rang [0, 10].
#

# 1.- Se validan los argumentos.

# Si la cantidad de argumentos introducidos es diferente de 1,

if [ $# -ne 1 ]
then
	
  # se muestran por pantalla textos en caso de error
  
  echo "Error: nombre d'arguments incorrecte."
  echo "Usage: $0 nota"

  # y se cierra el programa.

  exit $ERR_NARGS
fi

# 2.- Se valida el rango del argumento.

# Se introduce el argumento.

nota=$1

# Si la nota no pertenece al intervalo [0, 10],

if [ $nota -lt 0 -o $nota -gt 10 ]
then

  # Se muestra un mensaje de error.

  echo "Error: la nota introduïda ($nota) ha de ser un nombre entre 0 i 10."
  echo "Usage:"
  exit 1
fi

# 3.- El programa en sí.

# Si la nota introducida es mayor o igual a 5 se aprueba.
# Si no, se suspende.

if [ $nota -ge 5 ]
then
  echo "La nota introduïda ($nota) implica que s'ha aprovat."
else
  echo "La nota introduïda ($nota) implica que s'ha suspès."
fi
exit 0
