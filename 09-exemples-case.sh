#! /bin/bash
#
# Escola del Treball de Barcelona
# Administració de Sistemes Informàtics
# Curs 2021/2022
#
# Autor: Isaac Gordillo
# Data: 09/02/2022
#
# Descripció:
#
# Validar si l'argument introduït és un regular file, un directori
# o un link. Si és cap d'aquestes opcions, es tanca el programa.
# Si ho és, es llista.
#

case $1 in
  "pere"|"pau"|"joan")
    echo "És un nen."
    ;;
  "marta"|"anna"|"julia")
    echo "És una nena."
    ;;
  *)
    echo "És indefinit."
esac
