#! /bin/bash
#
# Escola del Treball de Barcelona
# Administració de Sistemes Informàtics
# Curs 2021/2022
#
# Autor: Isaac Gordillo
# Data: 14/02/2022
#
# Descripció:
#
# Validar si l'argument introduït és un regular file, un directori
# o un link. Si és cap d'aquestes opcions, es tanca el programa.
# Si ho és, es llista.
#

# 1.- Se valida el número de argumentos.

ERR_NARGS=1
ERR_MES=2
mes=$1

if [ $# -ne 1 ]
then

  # se muestran por pantalla textos en caso de error
  
  echo "Error: nombre d'arguments incorrecte."
  echo "Usage: $0 mes"

  # y se cierra el programa.

  exit $ERR_NARGS
fi

# 2.- Se valida el rango del argumento.

if [ $mes -lt 1 -o $mes -gt 12 ]
then

  # se muestra un mensaje de error.

  echo "ERROR: El mes ($1) ha de ser un nombre entre 1 i 12."
  echo "Usage: $0 mes"
  exit $ERR_MES
fi

# 3.- Determinar la cantidad de días que tiene un mes.

case $mes in

  "1"|"3"|"5"|"7"|"8"|"10"|"12")
    echo "El mes $mes té 31 dies."
    ;;
  "2")
    echo "El mes $mes té 28 dies."
    ;;
  *)
    echo "El mes $mes té 30 dies."
esac
